# MIP_cv4

## Popis řešení
V hlavním vlákně  se vykreslí GUI a zpracovává se klikání na displej.
Ve 2. vlákně se podle nastaveného stavu provádí práce pro daný mód (pouze vypsání módu na displej)


## Zadání
Simulujte chování autorádia, které má tři režimy: a) Posloucháme AM/FM, b) Posloucháme hudbu v MP3 z úložiště c) hovoříme přes hands-free d) rádio je vypnuté.

Na displeji zobrazte tyto ovládací prvky:
Zapnutí rádia (do posledního stavu před vypnutím, pamatuje si stav).
Vypnutí rádia
Aktivace AM/FM
Aktivace MP3
Příchozí hovor
Odchozí hovor

Ovládací prvky jsou vidět neustále, plus máme k dispozici část displeje, kde vidíme aktuální režim. Můžete měnit vzhled ovládacích prvků, abychom viděli, zda jsou “tlačítka” zmáčknuta nebo ne. To je na vás.

Část displeje, na němž je zobrazen režim, bude chráněna zámkem. Při odchodu ze stavu pro příslušný režim je zámek uvolněn, při vstupu do stavu je zapnut (lazy verze je chránit celý režim).

