#include "mbed.h"
#include "rtos.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include <list>

#define MODE_OFF 0
#define MODE_AM_FM 1
#define MODE_MP3_LOCAL 2
#define MODE_HANDS_FREE 3

#define BUTTON_WIDTH 75
#define BUTTON_HEIGHT 50
#define BUTTON_EDGE_OFFSET 30

TS_StateTypeDef TS_State;
volatile int state = MODE_OFF, last_state = MODE_AM_FM;
Semaphore one_slot(1, 1);

void drawButtons()
{
    uint8_t text[60];
    
    BSP_LCD_Clear(LCD_COLOR_BLACK);   
    BSP_LCD_SetTextColor(LCD_COLOR_YELLOW);
    BSP_LCD_SetFont(&Font16);
    
    int yPos =(BSP_LCD_GetYSize()/4)- (BUTTON_WIDTH/2);
    int xPos =BUTTON_EDGE_OFFSET;
    BSP_LCD_FillRect(xPos, yPos, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    xPos += BUTTON_WIDTH + BUTTON_EDGE_OFFSET;
    BSP_LCD_FillRect(xPos, yPos, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    xPos += BUTTON_WIDTH + BUTTON_EDGE_OFFSET;
    BSP_LCD_FillRect(xPos, yPos, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    xPos += BUTTON_WIDTH + BUTTON_EDGE_OFFSET;
    BSP_LCD_FillRect(xPos, yPos, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    yPos =(BSP_LCD_GetYSize()/4)*3;  
    xPos =2*BUTTON_EDGE_OFFSET;
    BSP_LCD_FillRect(xPos, yPos, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    xPos += BUTTON_WIDTH + 2*BUTTON_EDGE_OFFSET;
    BSP_LCD_FillRect(xPos, yPos, BUTTON_WIDTH, BUTTON_HEIGHT);
    
    
    BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
    BSP_LCD_SetTextColor(LCD_COLOR_CYAN);
    
    sprintf((char*)text, "     ON       OFF      AM/FM     MP3     ");   
    BSP_LCD_DisplayStringAt(0, LINE(1), (uint8_t *)&text, LEFT_MODE);
    
    sprintf((char*)text, "  INCOMING CALL    CALL    ");   
    BSP_LCD_DisplayStringAt(0, LINE(12), (uint8_t *)&text, LEFT_MODE);
}

bool touch()
{
    BSP_TS_GetState(&TS_State);
    if(TS_State.touchDetected) 
    {
        return true;
    }
    return false;   
}
void clear()
{
    uint8_t text[40];
    for(int i = 0; i< 40 ; i++)text[i] = ' ';
    BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)&text, CENTER_MODE);
}

void processTouch()
{
    uint8_t text[30];
    int x, y;

    BSP_TS_GetState(&TS_State);
    if(TS_State.touchDetected) 
    {
        x = TS_State.touchX[0];
        y = TS_State.touchY[0];    
        if(y >= BSP_LCD_GetYSize()/4 - (BUTTON_HEIGHT/2) && y <= BSP_LCD_GetYSize()/4 + (BUTTON_HEIGHT/2))
        {
            if(x >= BUTTON_EDGE_OFFSET && x <= BUTTON_EDGE_OFFSET + BUTTON_WIDTH)
            {
                if(state == MODE_OFF) state = last_state;
            }
            else if(x >= 2*BUTTON_EDGE_OFFSET + BUTTON_WIDTH && x <= 2*BUTTON_EDGE_OFFSET + 2*BUTTON_WIDTH)
            {
                if(state != MODE_OFF)
                {
                    last_state = state; 
                    state = MODE_OFF;
                }
            }
            else if(x >= 3*BUTTON_EDGE_OFFSET + 2*BUTTON_WIDTH && x <= 3*BUTTON_EDGE_OFFSET + 3*BUTTON_WIDTH)
            {
                if(state != MODE_OFF && state != MODE_AM_FM) state = MODE_AM_FM;
            }
            else if(x >= 4*BUTTON_EDGE_OFFSET + 3*BUTTON_WIDTH && x <= 4*BUTTON_EDGE_OFFSET + 4*BUTTON_WIDTH)
            {
                if(state != MODE_OFF && state != MODE_MP3_LOCAL) state = MODE_MP3_LOCAL;
            }

        }
        else if(y >= BSP_LCD_GetYSize()/4*3 && y <= BSP_LCD_GetYSize()/4*3 + (BUTTON_HEIGHT))
        {
            if(x >= 2*BUTTON_EDGE_OFFSET && x <= 2*BUTTON_EDGE_OFFSET + BUTTON_WIDTH)
            {
                if(state != MODE_OFF && state != MODE_HANDS_FREE) state = MODE_HANDS_FREE;
            }
            else if(x >= 4*BUTTON_EDGE_OFFSET + BUTTON_WIDTH && x <= 4*BUTTON_EDGE_OFFSET + 2*BUTTON_WIDTH)
            {
                if(state != MODE_OFF && state != MODE_HANDS_FREE) state = MODE_HANDS_FREE;
            }
        }
        HAL_Delay(200);
    }
    
}

void stateManager()
{
    uint8_t text[40]; 
    int currentState = MODE_OFF; 
    while(true)
    {
        if(currentState != state)
        {  
            one_slot.acquire();
            clear();   
            if(state == MODE_OFF)sprintf((char*)text, "RADIO IS OFF");  
            else if(state == MODE_AM_FM)sprintf((char*)text, "AM/FM mode");
            else if(state == MODE_MP3_LOCAL)sprintf((char*)text, "MP3 MODE");
            else if(state == MODE_HANDS_FREE)sprintf((char*)text, "HANDSFREE MODE");            
            BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)&text, CENTER_MODE);
            one_slot.release();
            currentState = state;         
        }
    }
}

int main() 
{   
    uint8_t text[40]; 
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
    
    uint8_t status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
    
    if (status != TS_OK) {
        BSP_LCD_Clear(LCD_COLOR_RED);
        BSP_LCD_SetBackColor(LCD_COLOR_RED);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)"TOUCHSCREEN INIT FAIL", CENTER_MODE);
    } else {
        BSP_LCD_Clear(LCD_COLOR_GREEN);
        BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
        BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *)"TOUCHSCREEN INIT OK", CENTER_MODE);
    }
    HAL_Delay(1000);
    
    drawButtons();
        
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_SetFont(&Font24);
    Thread t;
    t.start(stateManager);   
       
    while(1)
    {
        if(touch())
        {  
            processTouch();                    
        }
    }
}
